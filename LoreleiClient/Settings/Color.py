import pygame as pg


class ColorSettings:
    BACKGROUND_COLOR = (50, 50, 50)
    COLOR_INACTIVE = pg.Color('seashell1')
    COLOR_ACTIVE = pg.Color('yellow2')
    COLOR_PLAYER_MARKER = (0,201,87)