from LoreleiClient.Views.Modules.Common import Component
from LoreleiClient.UIObjects.Input import Box
from LoreleiClient.Settings.Color import ColorSettings
from LoreleiLib.Packets.Game.Map import RegionMapPacket, PlayerPositionPacket
from LoreleiLib.Map import MapNode
import pygame as pg


class MapNodeObject(object):

    GRID_SIZE = 16
    ROOM_SIZE = 10

    def __init__(self, node):
        # type: (MapNode) -> None
        self.uuid = node.uuid
        self.x = 500 + node.x * MapNodeObject.GRID_SIZE
        self.y = 500 - node.y * MapNodeObject.GRID_SIZE
        self.z = node.z
        self.exits = node.exits
        self.active = False

    def drawRoom(self, surface):
        color = ColorSettings.COLOR_INACTIVE
        if self.active:
            color = ColorSettings.COLOR_PLAYER_MARKER
        pg.draw.rect(surface, color, (self.x, self.y, MapNodeObject.ROOM_SIZE, MapNodeObject.ROOM_SIZE))

    def drawExits(self, surface):
        color = ColorSettings.COLOR_INACTIVE
        size = 2
        if self.active:
            color = ColorSettings.COLOR_PLAYER_MARKER
            size = 4

        start = (self.x + MapNodeObject.ROOM_SIZE/2, self.y + MapNodeObject.ROOM_SIZE/2)
        if self.exits[0] is True: # North
            pg.draw.line(surface, color, start, (start[0], start[1] - MapNodeObject.GRID_SIZE), size)
        if self.exits[1] is True:  # East
            pg.draw.line(surface, color, start, (start[0] + MapNodeObject.GRID_SIZE, start[1]), size)
        if self.exits[2] is True:  # South
            pg.draw.line(surface, color, start, (start[0], start[1] + MapNodeObject.GRID_SIZE), size)
        if self.exits[3] is True:  # West
            pg.draw.line(surface, color, start, (start[0] - MapNodeObject.GRID_SIZE, start[1]), size)


class MapRenderer(object):

    def __init__(self, x, y, renderSize, mapData):
        self.x = x
        self.y = y
        self.drag = False
        self.dragClick = None
        self.rect = pg.Rect(x, y, renderSize[0], renderSize[1])
        self.currentNode = None # type: MapNodeObject
        self.mapData = mapData
        self.cursor = (500-(renderSize[0]/2), 500-(renderSize[1]/2))
        self.renderSize = renderSize
        self.surfaceSize = (1000, 1000)
        self.surface = pg.Surface(self.surfaceSize)
        self.currentFloor = 0
        self.nodes = []
        self.floorNodes = {}
        self.nodeIds = {}
        self.changed = True
        self.setup()

    def setup(self):
        for z in self.mapData.floors: # type: dict
            for x in self.mapData.floors[z].keys(): # type: dict
                for y in self.mapData.floors[z][x].keys():
                    mapNode = self.mapData.floors[z][x][y] # type: MapNode
                    self.addNode(mapNode)

    def addNode(self, mapNode):
        # type: (MapNode) -> None
        mapNodeObj = MapNodeObject(mapNode)
        self.nodes.append(mapNodeObj)
        if not self.floorNodes.has_key(mapNode.z):
            self.floorNodes[mapNode.z] = []
        self.floorNodes[mapNode.z].append(mapNodeObj)
        self.nodeIds[mapNode.uuid] = mapNodeObj

    def draw(self, screen):
        if self.changed:
            self.surface.fill(ColorSettings.BACKGROUND_COLOR)
            for node in self.floorNodes[self.currentFloor]:
                if node is not None:
                    node.drawExits(self.surface)
            for node in self.floorNodes[self.currentFloor]:
                if node is not None:
                    node.drawRoom(self.surface)

            if self.currentNode is not None:
                self.currentNode.drawExits(self.surface)
                self.currentNode.drawRoom(self.surface)

            self.changed = False

        self.fixCursor()
        screen.blit(self.surface, (self.x, self.y, self.renderSize[0], self.renderSize[1]), (self.cursor[0], self.cursor[1], self.renderSize[0], self.renderSize[1]))

    def movedTo(self, uuid):
        if self.currentNode is not None:
            self.currentNode.active = False

        mapNode = self.nodeIds[uuid]
        mapNode.active = True
        self.currentFloor = mapNode.z
        self.currentNode = mapNode
        self.cursor = (self.currentNode.x - (self.renderSize[0]/2), self.currentNode.y - (self.renderSize[1]/2))
        self.fixCursor()
        self.changed = True

    def fixCursor(self):
        x = self.cursor[0]
        y = self.cursor[1]
        if x < 0:
            x = 0
        elif x + self.renderSize[0] > self.surfaceSize[0]:
            x = self.surfaceSize[0] - (self.renderSize[0] + 5)
        if y < 0:
            y = 0
        elif y + self.renderSize[1] > self.surfaceSize[1]:
            y = self.surfaceSize[1] - (self.renderSize[1] + 5)

    def handle_event(self, event):
        if event.type == pg.MOUSEBUTTONDOWN:
            if event.button == 1:
                if self.rect.collidepoint(event.pos):
                    self.drag = True
                    self.dragClick = event.pos

        elif event.type == pg.MOUSEBUTTONUP:
            if event.button == 1:
                self.drag = False

        elif event.type == pg.MOUSEMOTION:
            if self.drag:
                mouse_x, mouse_y = event.pos
                xDiff = -(mouse_x - self.dragClick[0])
                yDiff = -(mouse_y - self.dragClick[1])
                self.cursor = (self.cursor[0] + xDiff, self.cursor[1] + yDiff)
                self.fixCursor()
                self.changed = True
                self.dragClick = event.pos


class MapModule(Component):

    def __init__(self, x, y, mapData=None):
        self.mapData = mapData
        self.mapRenderer = None
        self.playerPosition = None
        super(MapModule, self).__init__(x, y, self.makeView)

    def makeView(self):
        if self.mapData is not None:
            outline = Box(self.x, self.y, 450, 200, 3, "Map")
            self.mapRenderer = MapRenderer(self.x, self.y, (outline.rect.w, outline.rect.h), self.mapData)
            self.add_ui_object(self.mapRenderer)
            self.add_ui_object(outline)

    def setMapData(self, mapData):
        self.mapData = mapData
        self.removeAll()
        self.makeView()
        if self.playerPosition is not None:
            self.mapRenderer.movedTo(self.playerPosition)

    def handleData(self, data):
        if isinstance(data, RegionMapPacket):
            self.setMapData(data.mapData)
        if isinstance(data, PlayerPositionPacket):
            self.playerPosition = data.uuid
            if self.mapRenderer is not None:
                self.mapRenderer.movedTo(data.uuid)