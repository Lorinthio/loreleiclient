from LoreleiClient.Views.Modules.Common import Component
from LoreleiClient.Views.Modules.Attributes import AttributeComponent
from LoreleiClient.Views.Modules.Equipment import EquipmentComponent
from LoreleiClient.Views.Modules.Statistics import StatisticsComponent
from LoreleiClient.Settings.Color import ColorSettings
import pygame as pg


class CharacterModule(Component):

    def __init__(self, x, y, connection, character):
        self.show = False
        self.connection = connection
        self.character = character
        self.attributes = AttributeComponent(54, 204, self.character) # 224 - 140 = 64
        self.equipment = EquipmentComponent(640, 70, self.character) # 444 bot, 940 right
        self.statistics = StatisticsComponent(54, 464, self.character) # 940-54 = 886, 464-240 = 224
        super(CharacterModule, self).__init__(x, y, self.makeView)

    def makeView(self):
        self.add_ui_object(self.statistics)
        self.add_ui_object(self.attributes)
        self.add_ui_object(self.equipment)

    def setCharacter(self, character):
        self.attributes.setCharacter(character)
        self.equipment.setCharacter(character)
        self.statistics.setCharacter(character)

    def handle_event(self, event):
        if event.type == pg.KEYDOWN:
            if event.key == pg.K_c:
                self.show = not self.show
        super(CharacterModule, self).handle_event(event)

    def draw(self, screen):
        if self.show:
            screen.fill(ColorSettings.BACKGROUND_COLOR)
            super(CharacterModule, self).draw(screen)